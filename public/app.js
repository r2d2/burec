var DataTable = function (options) {
    var _instance = $(options.selector).dataTable({
        "data": {},
        "bJQueryUI": true,
        "iDisplayLength": 25,
        "aoColumnDefs": options.aoColumnDefs,
        "sDom": 'T<"clear"><"fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>',
        "oTableTools": {
            "aButtons": ["csv"],
            "sSwfPath": "/swf/copy_csv_xls.swf"
        }
    });

    $(options.selector + ' tbody').on('click', 'tr', function (evt) {
        var data = _instance.fnGetData(evt.currentTarget);
        console.log('You clicked on row id: '+ data[0]);
    });

    // Filtering events
    $(options.selector + ' tfoot th.visible').each( function (f, node) {
        var title = $(options.selector + ' thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+ title + '" />' );
        
    });
 
    $(options.selector + ' tfoot input').keyup( function () {
        /* Filter on the column (the index) of this element */
        _instance.fnFilter( this.value, $(options.selector + ' tfoot input').index(this) + 1 );
    });

    return {
        //Instance API
        clear: function () {
            _instance.fnClearTable();
        },
        addData: function (data) {
            _instance.fnAddData(data);
        }
    };
};


var Pinger = function (ajaxConfig, controlsConfig, dataTableConfig, progressBarConfig) {
    var $startBtn = $("#start-" + controlsConfig.resourceType + "-ping").button();
    var $stopBtn = $("#stop-" + controlsConfig.resourceType + "-ping").button({
        disabled: true
    });

    var $radioButtons = $(controlsConfig.selector + ' span.radio');

    var checkIfSelected = function (a,b) {
        if (a === b) {
            return 'checked="checked"';
        }
        return '';
    };

    var getActiveCloudVersions = function(cb, err) {
        $.ajax({
            url: "/" + controlsConfig.resourceType + "/active_cloud_versions",
            type: "GET",
            cache: false,
            success: function (data) {
                cb(data);
            },
            error: function (message) {
                err(message);
            }
        });
    };

    
    var controls,
        dataTable,
        progressbar;

    getActiveCloudVersions(function (activeCloudVersions) {
        controlsConfig.activeCloudVersions = activeCloudVersions;

        $(controlsConfig.activeCloudVersions).each(function(index, cv) {
            var isSelected = checkIfSelected(cv, controlsConfig.defaultCloudVersion);
            $radioButtons.append('<input type="radio" name="' + controlsConfig.resourceType + '_cloud_version" value="' + cv+ '"' + isSelected +'/>'
            + cv);
        });

        dataTable = new DataTable(dataTableConfig);
        progressbar = new ProgressBar(progressBarConfig);
    }, function (error) {
        console.log("Something went wrong", error);
    });

    $startBtn.click(function () {
        $startBtn.button( "option", "disabled", true );
        $stopBtn.button( "option", "disabled", false );
        var cloudVersion = $(controlsConfig.selector + " .radio input[type='radio']:checked").val();
        
        dataTable.clear();
        progressbar.clear();
        $.when(getRecordIds(cloudVersion))
         .done(function (results, status) {
            progressbar.setSize(results.length);
            if (status === "success") {
                $.each(results, function (k,v) {
                    getPingData(v);
                });
            }
        });
    });

    $stopBtn.click(function () {
        $stopBtn.button( "option", "disabled", true );
        $startBtn.button( "option", "disabled", false );
        $.ajaxq.clear(ajaxConfig.queueName);
    });

    var getPingData = function (resourceId) {
        var random = Math.ceil(Math.random() * 400) + 25;
        setTimeout(function(){
            $.ajaxq( ajaxConfig.queueName, {
                url: "/" + controlsConfig.resourceType + "s/" + resourceId + "/ping",
                type: "GET",
                cache: false
            }).success(function (data) {
                progressbar.updateProgress();
                $.each(data, function(k, record) {
                    dataTable.addData(record);
                });
                //if ($.ajaxq.isQueueEmpty(ajaxConfig.queueName)) {
                    console.log("Queue empty: ", $.ajaxq.isQueueEmpty(ajaxConfig.queueName));
                    //$startBtn.button( "option", "disabled", false );
                //}
            }).error(function () {
                console.log("xhr error");
            });

            //re-enable start btn
            
        }, random);
    }

    var getRecordIds = function (cv) {
        return $.ajax({
            url: "/" + controlsConfig.resourceType + "/active_ids_by_cloud_version",
            data: {
                "cloud_version": cv
            },
            type: "GET",
            cache: false
        });
    };
};

var ProgressBar = function (options) {
    var barSize = null;

    var progressbar = $(options.selector),
      progressLabel = $(options.selector + "-label");

    progressbar.progressbar({
      value: 0,
      change: function(extraData) {
        progressLabel.text("Pinging" + progressbar.progressbar("value") + " of " + barSize );
      },
      complete: function() {
        progressLabel.text("Complete!");
      }
    });

    var updateProgress = function () {
        var val = progressbar.progressbar( "value" ) || 0;
        if ( val < barSize ) {
            progressbar.progressbar( "value", val + 1 );
        }
    }

    return {
        setSize: function (size) {
            progressbar.progressbar( "option", "max", size );
            barSize = size;
        },
        clear: function () {
             progressbar.progressbar('option', 'value', 0);
        },
        updateProgress: updateProgress
    };
};

$(document).ready(function() {
    var hostPinger = new Pinger({
        queueName: "host-ping-queue"
    },{
        selector: "#host-table-controls",
        resourceType: "host",
        defaultCloudVersion: 4
    },{
        selector: "#host-table",
        aoColumnDefs: [{ "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }]
    },{
        selector: "#host-progressbar"
    });

    var vmPinger = new Pinger({
        queueName: "vm-ping-queue"
    },{
        selector: "#vm-table-controls",
        resourceType: "vm",
        defaultCloudVersion: 4
    },{
        selector: "#vm-table",
        aoColumnDefs: [{ "bSearchable": false, "bVisible": false, "aTargets": [ 0 ] }]
    },{
        selector: "#vm-progressbar"
    });
});