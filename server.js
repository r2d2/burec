var Hapi = require("hapi");
var Chance = require('chance'),
    chance = new Chance();

var server = new Hapi.Server(8000, "localhost");
var id = 0;


server.route({
    path: "/{type}/active_cloud_versions",
    method: "GET",
    handler: function(request, reply) {
        reply([1,2,3,4]);
    }
});

//fake db calls
var vms = {
    "1": 30,
    "2": 16,
    "3": 34,
    "4": 20
};

var hosts = {
    "1": 10,
    "2": 30,
    "3": 20,
    "4": 15
};

server.route({
    path: "/host/active_ids_by_cloud_version",
    method: "GET",
    handler: function(request, reply) {
        var cloudVersion = request.query["cloud_version"] || [];
        var tmp = [];
        for (var i = 0; i < hosts[cloudVersion]; i++) {
            tmp.push(chance.integer({min: 1, max: 1000}));
        };
        reply(tmp);
    }
});

server.route({
    path: "/vm/active_ids_by_cloud_version",
    method: "GET",
    handler: function(request, reply) {
        var cloudVersion = request.query["cloud_version"] || [];
        var tmp = [];
        for (var i = 0; i < vms[cloudVersion]; i++) {
            tmp.push(chance.integer({min: 1, max: 1000}));
        };

        reply(tmp);
    }
});

server.route({
    path: "/hosts/{id}/ping",
    method: "GET",
    handler: function(request, reply) {
        var resourceId = request.params.id;
        var tmp = [];

        for (var i = 0; i < 100; i++) {
            tmp.push([
                //id: id++,
                id++,
                //cluster: chance.word(),
                chance.word(),
                //name: chance.name(),
                chance.name(),
                //ip: chance.ip(),
                chance.ip(),
                //domain: chance.domain(),
                chance.domain(),
                //status: chance.bool()
                chance.bool()
            ]);
        };

        reply(tmp);
        /*reply([
            "trc00",
            "",
            ""
            "x.x.x.x",
            "",
            "up"
        ]);*/
    }
});

server.route({
    path: "/vms/{id}/ping",
    method: "GET",
    handler: function(request, reply) {
        var id = request.params.id;
        var tmp = [];

        for (var i = 0; i < 100; i++) {
            tmp.push([
                //id: id++,
                id,
                //cluster: chance.word(),
                chance.word(),
                //host: chance.word(),
                chance.word(),
                //name: chance.name(),
                chance.name(),
                //ip: chance.ip(),
                chance.ip(),
                //domain: chance.domain(),
                chance.domain(),
                //status: chance.bool()
                chance.bool()
            ]);
        };

        reply(tmp);
    }
});

server.route({
    path: "/{path*}",
    method: "GET",
    handler: {
        directory: {
            path: "./public"
        }
    }
});

server.start(function () {
	console.log("Hapi server started @", server.info.uri);
});